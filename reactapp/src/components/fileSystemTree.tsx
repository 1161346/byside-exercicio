import * as React from "react";
// import { Folder } from "src/model/folder";

interface FileSystemTreeProps {
  fileSys: any;
  showTree: boolean;
}

class FileSystemTree extends React.Component<FileSystemTreeProps, {}> {
  state = {
    fsTree: this.props.fileSys,
    folderList: ["root/"],
    showTree: this.props.showTree
  };

  render() {
    return (
      <React.Fragment>
        <br />

        <div className="card">
          <div className="card-header">File System</div>
          <div className="card-body">
            <button
              type="button"
              className="btn btn-primary"
              onClick={this.drawTree}
            >
              Show Tree
            </button>

            <ul className="list-group list-group-flush">
              {" "}
              <br />
              {this.renderTree()}
            </ul>
          </div>
        </div>
      </React.Fragment>
    );
  }

  drawTree = (event: any) => {
    event.preventDefault();
    let tree = this.state.fsTree.getFileSystem();
    let folderPath = "";
    let treeArray: string[] = [];

    tree.walk(function(node: any) {
      // Halt the traversal by returning false

      let path = node.getPath();
      folderPath = "";
      for (let folder of path) {
        folderPath += folder.model.name.name + "/";
      }
      console.log(folderPath);

      treeArray.push(folderPath);
    });

    this.setState({
      folderList: this.state.folderList = treeArray
    });
    console.log(this.state.showTree);
  };

  renderTree() {
    const { folderList } = this.state;

    return folderList.length == 0 ? (
      <h1>No Tree</h1>
    ) : (
      folderList.map(folder => <li className="list-group-item">{folder}</li>)
    );
  }
}

export default FileSystemTree;
