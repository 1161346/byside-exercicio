import * as React from "react";
import { FileSystem } from "src/model/fileSystem";
import { Folder } from "src/model/folder";
//import { Folder } from "src/model/folder";

class FileSystemUI extends React.Component {
  public state = {
    fs: new FileSystem(),
    newFolder: "",
    showTree: false,
    folderList: ["root/"]
  };

  handleNewFolderChange = (event: { target: { value: any } }) => {
    this.setState({
      newFolder: event.target.value
    });
  };

  commandParser = (event: any) => {
    event.preventDefault();
    const { fs } = this.state;

    let command = this.state.newFolder;

    let commandVec = command.split(" ");

    switch (commandVec[0]) {
      case "mkdir": {
        fs.addFolder(commandVec[1]);
        break;
      }
      case "touch": {
        fs.addFile(commandVec[1]);

        break;
      }
      case "mv": {
        fs.moveFolder(commandVec[1], commandVec[2]);
        break;
      }
      case "mvf": {
        fs.moveFile(commandVec[1], commandVec[2]);
        break;
      }
      case "rm": {
        fs.removeFolder(commandVec[1]);
        break;
      }
      case "rmf": {
        fs.removeFile(commandVec[1]);
        break;
      }
      default: {
        //statements;
        break;
      }
    }
    this.drawTree(event);
  };
  public render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-md-6 offset-md-2">
            <input
              type="text"
              name="title"
              className="form-control"
              value={this.state.newFolder}
              onChange={this.handleNewFolderChange}
            />
          </div>
          <div className="col-md-3">
            <button
              type="button"
              className="btn btn-primary"
              onClick={this.commandParser}
            >
              Go
            </button>
          </div>
        </div>

        <div className="row">
          <div className="col-md-7  offset-md-2">
            <br />

            <div className="card">
              <div className="card-header">File System</div>
              <div className="card-body">
                <ul className="list-group list-group-flush">
                  {this.renderTree()}
                </ul>
              </div>
            </div>
          </div>
        </div>
        <br />
        <div className="row">
          <div className="col-md-7 offset-md-2">
            <div className="card bg-light mb-3">
              <div className="card-body">
                Criar pasta - mkdir [nome pasta]
                <br />
                Mover pasta - mv [pasta a mover] [pasta destino]
                <br />
                Remover pasta - rm [pasta]
                <hr />
                Criar ficheiro - touch [ficheiro] | touch [pasta pai]/[ficheiro]
                <br />
                Mover ficheiro - mvf [pasta pai]/[ficheiro a mover] [pasta
                destino]
                <br />
                Remover ficheiro - rmf [ficheiro] | rmf [pasta pai]/[ficheiro]
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }

  drawTree = (event: any) => {
    event.preventDefault();
    let tree = this.state.fs.getFileSystem();
    let folderPath = "";
    let treeArray: string[] = [];

    tree.walk(function(node: any) {
      // Halt the traversal by returning false

      let path = node.getPath();
      let newFolder: Folder = new Folder("mock");
      folderPath = "";
      for (let folder of path) {
        folderPath += folder.model.name.name + "/";

        newFolder = folder.model.name;
        console.log("newFolder");
        console.log(newFolder);
      }
      console.log(newFolder);
      if (newFolder.listFile.length == 0 && newFolder.name != "mock") {
        folderPath += "  -  Empty";
      }
      if (newFolder.listFile.length > 0 && newFolder.name != "mock") {
        for (let file of newFolder.listFile) {
          folderPath += " - " + file.name;
        }
      }
      console.log(folderPath);

      treeArray.push(folderPath);
    });

    this.setState({
      folderList: this.state.folderList = treeArray
    });
    console.log(this.state.showTree);
  };

  renderTree() {
    const { folderList } = this.state;

    return folderList.length == 0 ? (
      <h1>No Tree</h1>
    ) : (
      folderList.map(folder => <li className="list-group-item">{folder}</li>)
    );
  }
}

export default FileSystemUI;
