import * as TreeModel from "tree-model";
import { Folder } from "./folder";

export class FileSystem {
  public tree: any;
  private treeModel: any;

  constructor() {
    this.treeModel = new TreeModel();

    this.tree = this.treeModel.parse({
      name: new Folder("root"),
      children: []
    });

    console.log(this.tree);
  }

  addFolder(name: string) {
    let nameVec: string[] = name.split("/");

    if (nameVec.length == 2) {
      let parentFolder: string = nameVec[0];
      let newFolderName: string = nameVec[1];

      var parentNode = this.tree.first(function(node: any) {
        return node.model.name.name === parentFolder;
      });

      let newNode = this.treeModel.parse({ name: new Folder(newFolderName) });
      parentNode.addChild(newNode);
    } else {
      let newNode = this.treeModel.parse({ name: new Folder(name) });
      this.tree.addChild(newNode);
    }
  }
  addFile(name: string): any {
    let nameVec: string[] = name.split("/");

    if (nameVec.length == 2) {
      let parentFolder: string = nameVec[0];
      let newFileName: string = nameVec[1];

      var parentNode = this.tree.first(function(node: any) {
        return node.model.name.name === parentFolder;
      });

      let folderAdd = parentNode.model.name;

      folderAdd.addFile(newFileName);
      console.log(folderAdd);
    } else {
      var rootNode = this.tree.first(function(node: any) {
        return node.model.name.name === "root";
      });

      let rootFolderAdd = rootNode.model.name;

      rootFolderAdd.addFile(name);
      console.log(rootFolderAdd);
    }
  }

  removeFolder(folderName: string) {
    var node = this.tree.first(function(node: any) {
      return node.model.name.name === folderName;
    });

    node.drop();
  }

  moveFolder(originFolder: string, destFolder: string) {
    // let folderList: Folder[] = [];
    // let folderNameList: string[] = [];

    var nodeFolder = this.tree.first(function(node: any) {
      return node.model.name.name === originFolder;
    });

    var nodeDest = this.tree.first(function(node: any) {
      return node.model.name.name === destFolder;
    });

    nodeDest.addChild(nodeFolder.drop());
  }

  removeFile(name: string) {
    console.log("remove file");
    let nameVec: string[] = name.split("/");

    if (nameVec.length == 2) {
      let parentFolder: string = nameVec[0];
      let newFileName: string = nameVec[1];

      var parentNode = this.tree.first(function(node: any) {
        return node.model.name.name === parentFolder;
      });

      let folderRemove = parentNode.model.name;

      folderRemove.removeFile(newFileName);
      console.log(folderRemove);
    } else {
      this.tree.model.name.removeFile(name);
    }
  }

  moveFile(originFolder: string, destFolder: string) {
    console.log("move file");
    let nameVec: string[] = originFolder.split("/");
    let nameVecDest: string[] = destFolder.split("/");

    if (nameVec.length == 2) {
      let parentFolder: string = nameVec[0];
      let newFileName: string = nameVec[1];

      let destFolder: string = nameVecDest[0];

      var parentNode = this.tree.first(function(node: any) {
        return node.model.name.name === parentFolder;
      });

      var destNode = this.tree.first(function(node: any) {
        return node.model.name.name === destFolder;
      });

      let folderRemove = parentNode.model.name;
      let folderDest = destNode.model.name;

      let folderRm = folderRemove.removeFile(newFileName);

      if (folderRm.name != "mock") {
        folderDest.addFile(folderRm.name);
      }

      console.log(folderRemove);
      console.log(folderDest);
    }
  }

  getFileSystem() {
    return this.tree;
  }
}
