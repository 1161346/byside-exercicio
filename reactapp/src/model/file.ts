export class File {
  public name: string;
  public permissions: string;
  public content: string;

  constructor(name: string) {
    this.name = name;
    this.permissions = "rwx;rwx;r-w";
    this.content = "NEW FILE";
  }
}
