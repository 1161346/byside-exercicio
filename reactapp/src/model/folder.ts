import { File } from "./file";

export class Folder {
  public name: string;
  public permissions: string;
  public listFile: File[] = [];

  constructor(name: string) {
    this.name = name;
    this.permissions = "rwx;rwx;r-w";
  }

  addFile(name: string) {
    this.listFile.push(new File(name));
  }

  removeFile(name: string) {
    let folderIndex = -1;
    let file: File = new File("mock");

    for (let i = 0; i < this.listFile.length; i++) {
      if (this.listFile[i].name == name) {
        console.log("its u");
        console.log(this.listFile[i].name);
        console.log(name);
        file = this.listFile[i];
      }
    }
    console.log(file);

    if (file.name != "mock") {
      folderIndex = this.listFile.indexOf(file);

      if (folderIndex != -1) {
        this.listFile.splice(folderIndex, 1);
      }

      return file;
    }
    return file;
  }
}
