import * as React from "react";
import "./App.css";

import logo from "./logo.svg";
import FileSystemUI from "./components/fileSystem";

class App extends React.Component {
  public render() {
    return (
      <div className="container">
        <br />

        <header className="jumbotronx">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">File System</h1>
        </header>
        <br />
        <br />
        <FileSystemUI />
      </div>
    );
  }
}

export default App;
